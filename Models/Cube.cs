﻿namespace Blaztermind.Models;

public class Cube
{
    public string Color { get; set; }
    public ValidationColor ValidationColor { get; set; }
}