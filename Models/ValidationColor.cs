﻿namespace Blaztermind.Models;

public class ValidationColor
{
    public string Color { get; set; }

    public string Legend
    {
        get
        {
            return Color switch
            {
                "White" => "La couleur n'est pas/plus dans la combinaison",
                "Red" => "La couleur est mal positionnée",
                _ => "La couleur est bien positionnée"
            };
        }
    }
}