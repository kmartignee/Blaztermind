﻿using Blaztermind.Models;

namespace Blaztermind.Aggregates;

public class BlaztermindAggregate
{
    public List<Cube> ValidationCubes { get; set; } = new();
    public List<Cube> PlayingCubes { get; set; } = new();
    public List<List<Cube>> HistoryListCube { get; set; } = new();
    public List<string> AvailableColors { get; set; } = new();
    public bool Cheat { get; set; }
    public bool IsEndGame { get; set; }
    public bool IsWin { get; set; }
    public bool IsStartGame { get; set; }
    public bool IsCustomGame { get; set; }
    public string? CustomNumberCube { get; set; }
    public string? CustomNumberColor { get; set; }
    public string? EndMessage { get; set; }
}