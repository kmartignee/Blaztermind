namespace Blaztermind.Enums;

public enum ColorEnum
{
    Red,
    Blue,
    Green,
    Yellow,
    Orange,
    Purple,
    Brown,
    White,
    Black
}