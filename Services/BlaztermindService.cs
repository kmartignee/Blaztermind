﻿using Blaztermind.Aggregates;
using Blaztermind.Enums;
using Blaztermind.Models;

namespace Blaztermind.Services;

public class BlaztermindService
{
    private readonly BlaztermindAggregate _aggregate;
    
    public BlaztermindService(BlaztermindAggregate aggregate)
    {
        _aggregate = aggregate;
    }

    // Méthode pour démarrer le jeu avec une difficulté donnée
    public void StartGame((int numberOfCube, int numberOfColor) difficulty)
    {
        _aggregate.IsStartGame = false;

    // Initialise les couleurs disponibles, les cubes de validation et les cubes de jeu
        _aggregate.AvailableColors = GetAvailableColors(difficulty.numberOfColor);
        _aggregate.ValidationCubes = GetValidationCubes(difficulty.numberOfCube);
        _aggregate.PlayingCubes = GetPlayingCubes(difficulty.numberOfCube);
    }

    // Méthode pour obtenir la liste des couleurs disponibles en fonction du nombre de couleurs spécifié
    public static List<string> GetAvailableColors(int numberOfColor)
    {
        var colors = new List<string>();

        for (var i = 0; i < numberOfColor; i++)
            colors.Add(((ColorEnum)i).ToString());

        return colors;
    }

    // Méthode pour générer une liste de cubes de validation avec des couleurs aléatoires
    public List<Cube> GetValidationCubes(int cubeNumber)
    {
        var cubes = new List<Cube>();

        for (var i = 0; i < cubeNumber; i++)
            cubes.Add(new Cube { Color = RandomColor() });

        return cubes;
    }

    // Méthode pour générer une liste de cubes de jeu avec une couleur prédéfinie
    public List<Cube> GetPlayingCubes(int cubeNumber)
    {
        var cubes = new List<Cube>();

        for (var i = 0; i < cubeNumber; i++)
            cubes.Add(new Cube { Color = _aggregate.AvailableColors.First()});

        return cubes;
    }

    // Méthode pour obtenir une couleur aléatoire parmi les couleurs disponibles
    private string RandomColor()
    {
        var colors = new List<string>();

        colors.AddRange(_aggregate.AvailableColors);

        var random = new Random();
        var index = random.Next(colors.Count);

        return colors[index];
    }

    // Méthode pour vérifier si il reste une autre couleur identique existe dans les cubes de validation
    private bool CheckIfThereIsAnOtherSameColorLeftInValidationCubes(string color, int index)
    {
        var range = _aggregate.PlayingCubes.GetRange(0, index);
        var numberOfSameColor = range.Count(x => x.Color == color);
        return numberOfSameColor <= _aggregate.ValidationCubes.Count(x => x.Color == color);
    }

    // Méthode pour obtenir la couleur de validation pour un cube donné
    public string GetValidationColor(Cube cube, int cubesCount, int index)
    {
        var exist = _aggregate.ValidationCubes.Exists(x => x.Color == cube.Color);
        var isRightPositioned = _aggregate.ValidationCubes[cubesCount].Color == cube.Color;
        var otherSameColorLeft = CheckIfThereIsAnOtherSameColorLeftInValidationCubes(cube.Color, index);

        if (!exist || !otherSameColorLeft)
            return "White";
        
        if (!isRightPositioned)
            return "Red";

        return "Yellowgreen";
    }

    // Méthode pour vérifier si le joueur a gagné en validant tous les cubes
    public void CheckIfWin(List<Cube> cubes, int tries)
    {
        var win = cubes.All(x => x.ValidationColor.Color == "Yellowgreen");

        if (!win)
            return;

        _aggregate.IsEndGame = true;
        _aggregate.IsWin = true;
        _aggregate.EndMessage = $"Bravo, vous avez gagné en {tries} essais !";
    }

    // Méthode pour vérifier si le joueur a perdu en ne validant pas tous les cubes après 7 tentatives
    public void CheckIfLose(List<List<Cube>> historyCubes)
    {
        var lose = historyCubes.Count == 7 &&
                   historyCubes.Last().Any(x => x.ValidationColor.Color != "Yellowgreen");

        if (!lose)
            return;

        _aggregate.IsEndGame = true;
        _aggregate.IsWin = false;
        _aggregate.EndMessage = "Dommage, vous avez perdu !";
    }
}